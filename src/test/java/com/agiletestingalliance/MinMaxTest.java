package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;
import com.agiletestingalliance.MinMax;

public class MinMaxTest {

	@Test
	public void testDescForB() throws Exception {
		int res = new MinMax().checkMax(3,4);
		assertEquals("B Greater",4, res);
	}
	
	@Test
	public void testDescForA() throws Exception {
		int res = new MinMax().checkMax(4,3);
		assertEquals("A Greater",4, res);
	}
}
